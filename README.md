# [alpine-x64-docker-registry](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-docker-registry/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-docker-registry.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-docker-registry "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-docker-registry.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-docker-registry "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-docker-registry.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-docker-registry/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-docker-registry.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-docker-registry/)



----------------------------------------
#### Description
* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Base Image   : [forumi0721alpinex64/alpine-x64-base](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-base/)
* Appplication : docker-registry
    - The Registry is a stateless, highly scalable server side application that stores and lets you distribute Docker images.



----------------------------------------
#### Run
```sh
docker run -d \
           -p 5000:5000/tcp \
           -v /repo:/repo \
           forumi0721alpinex64/alpine-x64-docker-registry:latest
```



----------------------------------------
#### Usage
* push
```
docker push localhost:5000/<repository>
```

* pull
```
docker pull localhost:5000/<repository>
```



----------------------------------------
#### Docker Options
| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables
| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes
| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /repo              | Persistent data                                  |


#### Ports
| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 5000/tcp           | Listen port for reistory daemon                  |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-docker-registry](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-docker-registry/)

